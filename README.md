# NEPSE (NEPAL STOCK EXCHANGE)

The Nepal Stock Exchange is the **only stock exchange of Nepal**. It is located in Singha Durbar Plaza, Kathmandu, Nepal. As of 8 February 2021, the equity market capitalization of the companies **listed** on NEPSE was approximately **US$30 billion**. It is regulated by the Securities Board of Nepal **(SEBON)**. As of 16 March 2021, the number of listed companies is **285**.

## Types of Shares
      1. Primary Shares(IPO) : shares, debentures or bonds, and mutual funds are   issued directly by a company at a base price.
      2. Seconday Shares: Which one can buy at offered price.
     
```Why to buy shares of any kind```
   - **for capital gain**,
   - **to receive dividend**.
 
```Form of Dividend (Received if shareholder until Book Closing Date)```
   - **Cash Bonus**,
   - **Bonus Shares**.
   - **Cash + Share Bonus**.
   - **Apart from this A company may issue Right shares**.
   - **FPO: Issued by companies to generate revenue**
   ** all Are subjected to Tax, commission, DP charges upon sales regardless of profit(except tax(7.5% or 5%)
 
``How to easily start get into Market ``
   1. Opening Demat Account and Getting meroshare login credentials(they will be charged annually).
   2. Opening tms Account from any of the brokers.
   3. And simply start trading and lose money.
  
``How to buy and sell shares ``
   1. You can obtain login credentials for the NEPSE Trade Management System portal, from which you can trade shares directly. Cash transactions are also online here.
   2. You can opt for offline transactions, in which you have to be either physically present or contact brokers through phones for your desired transactions. For this, you need to issue an account payee cheque in the name of the broker company.
**don't forget to do EDIS after selling stocks**

[![Demo tms ](./Video_1642678323.wmv)]


```How do you earn with shares?```
- Active Trading: 
- Passive Income(dividend CAGR)


``Analysis``
  - **Fundamental Analysis**
   Fundamental analysis is a method of determining a stock's real or "fair market" value
    - Reserve
    - PE ratio
    - ROA (return on Assets= (operating expanses or net profit)/total assets) ![ROA](./ROA.jpg)
    - ROE(return on equity): measure of profitability of a business in realtion to the equity.= net profit/total equity(shares) ![ROE](./ROE.jpg)
  
    - **Technical Analysis**
   Technical analysis is a method of determining a stock's probable price in near future and the way to maximize the gain from it from it's past price performance
    - Based on History Repeat Itself ![Hisoty Repeat itself](./HRI.jpg)
    - Trend Theory - Upward (Bullish), downward(Bearish) trend, sideways - Trend Reversal/Breakout, home run return ![Trendline](./trendline.jpg) , ![uptrend](./uptrend.jpg) , ![downtrend](./downtrend.jpg) , ![downtrend](./sideways.jpg), ![downtrend](./breakout.jpg), 
    - Charts - candles ![downtrend](./candles.jpg),  ![downtrend](./bullishcandle.jpg),  ![downtrend](./bearishcandle.jpg),  ![downtrend](./hammar.jpg),  ![downtrend](./bottomhammer.jpg),  ![downtrend](./hangingman.jpg),  ![downtrend](./invertedhammer.jpg),  
    - Support and Resistance
    - Moving Average
    - Elliot Wave Theory
    - Indicators
    - Chart Patterns
    - Trading Volume
    - RSI ![downtrend](./rsicalculation.jpg), ![downtrend](./rsizone.jpg), ![downtrend](./longrsi.jpg),  ![downtrend](./strongrsi.jpg),   
    - MACD (Moving Average Convergence and Divergence) macd line(blue) and signal line(red line), lines are histogram
    - Fibonacci Retracement
    - Ichimoku cloud
    - FloorSheet
    - Market Capitals
    - Bollingers band ![bollingers bands](./bollingersband.jpg) ![bollingers bands](./bollinvolatility.jpg)  ![bollingers bands](./bollmoving.jpg)

``Risk Reward Ratio``
  - **Risk Beaking Capacity**
   ![downtrend](./riskreward.jpg) ,
   ![downtrend](./riskrewardratio.jpg) ,
   ![downtrend](./riskrrew.jpg) ,
   ![downtrend](./points.jpg) ,

